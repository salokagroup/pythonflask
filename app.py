from flask import Flask, request, render_template, url_for, redirect, session, flash
from werkzeug.utils import secure_filename
from koneksi import db

app = Flask(__name__)
app.secret_key = "raji@#$63"

# create routing index
@app.route('/')
def index():
  return render_template('index.html')

@app.route('/login', methods=['POST', 'GET'])
def login():
  message = " "
  if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
    username = request.form['username']
    password = request.form['password']
    
    cursor = db.cursor()
    query = "SELECT * FROM user WHERE username=%s AND password=%s"
    val = (username, password)
    cursor.execute(query, val)
    account = cursor.fetchone()
    if account:
      flash("login successfull")
      return redirect(url_for('dashboard'))
    else:
      flash('anda gagal login username & password salah')
      return redirect(url_for('login'))

  return render_template('login.html')

@app.route('/register', methods=['POST', 'GET'])
def register():
  if request.method == 'POST':
    nama = request.form['nama']
    username = request.form['username']
    password = request.form['password']

    cursor = db.cursor()
    query = "INSERT INTO user (nama, username, password) VALUES (%s, %s, %s)"
    data = (nama, username, password)
    cursor.execute(query, data)
    db.commit()
    cursor.close()

    if cursor:
      flash ("register berhasil")
      return redirect(url_for('login'))
    else:
      flash ("register gagal")
      return redirect(url_for('register'))
  return render_template('register.html')


@app.route('/logout', methods = ['POST', 'GET'])
def logout():
   session.pop('id', None)
   session.pop('username', None)
   return redirect(url_for('index'))

@app.route('/mahasiswa/daftar_alumni', methods=['POST', 'GET'])
def alumni():
  if request.method == 'POST':
    nama = request.form['nama']
    nim = request.form['nim']
    jurusan = request.form['jurusan']
    tahun_masuk = request.form['tahun_masuk']
    tahun_keluar = request.form['tahun_keluar']
    no_hp = request.form['no_hp']
    status = request.form['status']

    cursor = db.cursor()
    sql = "INSERT INTO alumni(nama, nim, jurusan, tahun_masuk, tahun_keluar, no_hp, status) VALUES(%s, %s, %s, %s, %s, %s, %s)"
    data = (nama, nim, jurusan, tahun_masuk, tahun_keluar, no_hp, status,)
    cursor.execute(sql, data)
    db.commit()
    cursor.close()

    if cursor:
      flash ("data alumni berhasil disimpan")
      return redirect(url_for('alumni'))
  return render_template('mahasiswa/mhs.html')

@app.route('/mahasiswa/info')
def info():
  cursor = db.cursor()
  query = "SELECT * FROM berita"
  cursor.execute(query)
  result = cursor.fetchall()
  return render_template('mahasiswa/berita.html', value=result)

@app.route('/pengajaran/dashboard', methods=['POST', 'GET'])
def dashboard():
  cursor = db.cursor()
  query = "SELECT * FROM alumni"
  cursor.execute(query)
  result = cursor.fetchall()
  return render_template('pengajaran/dashboard.html', value=result)

@app.route('/delete/<int:id_alumni>')
def delete(id_alumni):
    cursor = db.cursor()
    cursor.execute("DELETE FROM alumni WHERE id_alumni = %s", (id_alumni,))
    db.commit()
    cursor.close()
    return redirect(url_for('dashboard'))

@app.route('/pengajaran/lihat_berita', methods=['POST', 'GET'])
def lihat_berita():
  cursor = db.cursor()
  query = "SELECT * FROM berita"
  cursor.execute(query)
  result = cursor.fetchall()
  return render_template('pengajaran/lihat_berita.html', value=result)

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
  
def allowed_file(filename):
 return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
  
@app.route('/pengajaran/buat_berita', methods=['POST', 'GET'])
def imageUpload():
  if request.method == 'POST':
    judul = request.form['judul']
    isi = request.form['isi']
    tanggal_upload = request.form['tanggal_upload']
    image = request.files['image']

    cursor = db.cursor()
    sql = "INSERT INTO berita (judul, isi,tanggal_upload, image) VALUES (%s, %s, %s, %s)"
    data = (judul, isi, tanggal_upload, imagem )
    result = cursor.execute(sql)
    db.commit()
    cursor.close()
    if result:
      flash ("data berhasil di masukkan")
      return redirect(url_for('lihat_berita'))
  return render_template('pengajaran/berita.html')

# running flask
if __name__ == '__main__':
  app.run(debug=True)
